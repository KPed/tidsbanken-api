FROM molgenis/maven-jdk17 AS maven
WORKDIR /app
COPY . .
RUN mvn clean install -DskipTests

FROM openjdk:17
WORKDIR /app
ENV SPRING_PROFILE production
ENV DATABASE_URL ""
ENV DDL_AUTO "update"
ENV ISSUER_URL "https://g3-keycloak.herokuapp.com/auth/realms/kc-react"
ENV JWKS_URI "https://g3-keycloak.herokuapp.com/auth/realms/kc-react/protocol/openid-connect/certs"
ENV CLIENT_ID ${CLIENT_ID}
ENV CLIENT_SECRET ${CLIENT_SECRET}
ENV APP_ORIGIN "http://localhost:3000"

COPY --from=maven ./app/target/tidsbanken-api-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "tidsbanken-api-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080