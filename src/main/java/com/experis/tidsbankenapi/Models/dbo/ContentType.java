package com.experis.tidsbankenapi.Models.dbo;

public enum ContentType {
    REQUEST,
    COMMENT;
}
