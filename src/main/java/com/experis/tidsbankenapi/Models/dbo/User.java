package com.experis.tidsbankenapi.Models.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Table(name="user_profile")
public class User {

    @Id
    private UUID id;

    @Column(length = 40)
    private String firstName;

    @Column(length = 40)
    private String lastName;

    @Column(length = 100)
    private String email;

    @Column
    private boolean isAdmin;

    @Column
    private Date lastLogin;

    public User(String email) {
        this.email = email;
    }

    public User(){
    }
    @JsonGetter(value = "requests")
    public List<String> getRequests(){
        if(requests == null) return null;
        return requests.stream().map(request -> {
            return String.format("/api/request/%s", request.getId());
        }).collect(Collectors.toList());
    }

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Request> requests;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Comment> comments;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Ineligible> ineligibles;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {this.id = id;}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
}
