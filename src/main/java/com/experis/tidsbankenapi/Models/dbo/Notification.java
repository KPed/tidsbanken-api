package com.experis.tidsbankenapi.Models.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.nimbusds.jose.shaded.json.JSONObject;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;


public class Notification {

    private final NotificationContent content;

    @Enumerated(EnumType.STRING)
    private ContentType type;

    private Date timeStamp;

    public Notification(NotificationContent content) {
        this.content = content;
    }


    public NotificationContent getContent() {
        return content;
    }

    public ContentType getType() {
        return type;
    }


    public void setType(ContentType type) {
        this.type = type;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
