package com.experis.tidsbankenapi.Models.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nimbusds.jose.shaded.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request extends NotificationContent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 40)
    private String title;

    @Column
    private Date periodStart;

    @Column
    private Date periodEnd;

    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;

    @Column
    private Date timeOfModeration;

    @Column(nullable = false)
    private Boolean seen;

    @JsonGetter(value = "user")
    public JSONObject getRequestUser(){
        if(user == null) return null;
        JSONObject partialUser = new JSONObject();
        partialUser.put("id",user.getId());
        partialUser.put("firstName",user.getFirstName());
        partialUser.put("lastName",user.getLastName());
        return partialUser;
    }

    @JsonGetter(value = "moderator")
    public JSONObject getRequestModerator(){
        if(moderator == null) return null;
        JSONObject partialModerator = new JSONObject();
        partialModerator.put("id", moderator.getId());
        return partialModerator;
    }

    @JsonGetter(value = "comments")
    public List<String> getComments(){
        if(comments == null) return null;
        return comments.stream().map(comment -> {
            return String.format("/api/request/%d/comment/%d", this.getId(),comment.getId());
        }).collect(Collectors.toList());
    }

    @JsonGetter(value = "comments")
    public List<String> getCommentById(Integer comment){
        if(comments == null) return null;
        return comments.stream().map(request -> {
            return String.format("/api/request/%d", request.getId());
        }).collect(Collectors.toList());
    }

    @ManyToOne
    @JoinColumn(name = "fk_owner_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "fk_moderator_id")
    private User moderator;

    @OneToMany(mappedBy = "request", fetch = FetchType.LAZY)
    private List<Comment> comments;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }

    public Date getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(Date periodEnd) {
        this.periodEnd = periodEnd;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public void setModerator(User moderator) {
        this.moderator = moderator;
    }

    public void setTimeOfModeration(Date timeOfModeration) {
        this.timeOfModeration = timeOfModeration;
    }

    public Date getTimeOfModeration() {
        return timeOfModeration;
    }

    public User getModerator() {
        return moderator;
    }

    public Boolean isSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }
}
