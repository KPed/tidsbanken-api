package com.experis.tidsbankenapi.Models.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nimbusds.jose.shaded.json.JSONObject;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ineligible {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Date periodStart;

    @Column
    private Date periodEnd;

    @JsonGetter(value = "user")
    public JSONObject getIneligibleUser(){
        if(user == null) return null;
        JSONObject partialUser = new JSONObject();
        partialUser.put("id",user.getId());
        partialUser.put("firstName",user.getFirstName());
        partialUser.put("lastName",user.getLastName());
        return partialUser;
    }

    @ManyToOne
    @JoinColumn(name = "fk_created_by")
    private User user;

    public Integer getId() {
        return id;
    }
    public Date getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }

    public Date getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(Date periodEnd) {
        this.periodEnd = periodEnd;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
