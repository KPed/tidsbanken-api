package com.experis.tidsbankenapi.Repositories;

import com.experis.tidsbankenapi.Models.dbo.Comment;
import com.experis.tidsbankenapi.Models.dbo.Request;
import com.experis.tidsbankenapi.Models.dbo.RequestStatus;
import com.experis.tidsbankenapi.Models.dbo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface IRequestRepository extends JpaRepository<Request,Integer> {
    Page<Request> getAllByUserOrRequestStatusOrderByPeriodStartAsc(User user, RequestStatus status, Pageable pageable);
    Page<Request> OrderByPeriodStartAsc(Pageable pageable);
    Request getRequestById(Integer requestId);
    List<Request> getAllByUser(User user);
    List<Request> getAllByTimeOfModerationIsAfterAndUser(Date date, User user);
    List<Request> getAllBySeenAndUser(Boolean seen, User user);


}
