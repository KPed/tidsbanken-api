package com.experis.tidsbankenapi.Repositories;

import com.experis.tidsbankenapi.Models.dbo.Comment;
import com.experis.tidsbankenapi.Models.dbo.Request;
import com.experis.tidsbankenapi.Models.dbo.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ICommentRepository extends JpaRepository<Comment,Integer> {
    List<Comment> getAllByRequestIdOrderByTimeStampDesc(Integer requestId);
    Optional<Comment> getCommentByRequestIdAndId(Integer requestId, Integer commentId);
    List<Comment> getAllByRequest(Request request);
    List<Comment> getAllByTimeStampAfterOrEditedAtTimeAfterAndRequest_User(Date timestamp,Date editstamp, User user);
    List<Comment> getAllBySeenAndRequest_User(Boolean seen, User user);
}
