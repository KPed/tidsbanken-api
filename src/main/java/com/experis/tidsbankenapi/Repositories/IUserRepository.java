package com.experis.tidsbankenapi.Repositories;

import com.experis.tidsbankenapi.Models.dbo.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface IUserRepository extends JpaRepository<User,Integer> {
    boolean existsById(UUID id);
    User getByEmail(String email);
    User getUserById(UUID id);
    Optional<User> findUserById(UUID id);
}

