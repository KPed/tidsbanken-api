package com.experis.tidsbankenapi.Controllers;

import com.experis.tidsbankenapi.Services.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@CrossOrigin("${server.cors.application_origin}")
@SecurityRequirement(name = "keycloak_implicit")
@RequestMapping("api")
public class LoginController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<String> login(@AuthenticationPrincipal Jwt principal){
        //User is authenticated from frontend
        //if user not in database add to database
        if(!userService.checkIfUserExists(UUID.fromString(principal.getSubject()))){
            userService.createNewUserFromJwt(principal);
        }
        return ResponseEntity.ok().build();
    }
}
