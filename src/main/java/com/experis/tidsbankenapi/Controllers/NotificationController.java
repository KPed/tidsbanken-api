package com.experis.tidsbankenapi.Controllers;


import com.experis.tidsbankenapi.Models.dbo.Comment;
import com.experis.tidsbankenapi.Models.dbo.ContentType;
import com.experis.tidsbankenapi.Models.dbo.Request;
import com.experis.tidsbankenapi.Models.dbo.User;
import com.experis.tidsbankenapi.Repositories.ICommentRepository;
import com.experis.tidsbankenapi.Repositories.IRequestRepository;
import com.experis.tidsbankenapi.Repositories.IUserRepository;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin("${server.cors.application_origin}")
@SecurityRequirement(name = "keycloak_implicit")
@RequestMapping("api")
public class NotificationController {

    private final IRequestRepository requestRepository;
    private final ICommentRepository commentRepository;

    public NotificationController(IRequestRepository requestRepository, ICommentRepository commentRepository) {
        this.requestRepository = requestRepository;
        this.commentRepository = commentRepository;
    }

    @PatchMapping("/notification/{contentType}/{contentId}")
    @PreAuthorize("hasAnyRole({'user', 'admin'})")
    public void seenNotification(@PathVariable ContentType contentType, @PathVariable Integer contentId, @RequestBody Boolean seen){
        if(contentType.equals(ContentType.REQUEST)){
            Request request = requestRepository.getRequestById(contentId);
            request.setSeen(seen);
            List<Comment> comments = commentRepository.getAllByRequest(request);
            for(Comment com: comments){
                com.setSeen(seen);
            }
            requestRepository.save(request);
        }
        else if(contentType.equals(ContentType.COMMENT)){
            Comment comment = commentRepository.getById(contentId);
            Request request = comment.getRequest();
            request.setSeen(seen);
            requestRepository.save(request);
            List<Comment> comments = commentRepository.getAllByRequest(request);
            for(Comment com: comments){
                com.setSeen(seen);
                commentRepository.save(com);
            }
        }

    }
}
