package com.experis.tidsbankenapi.Controllers;

import com.experis.tidsbankenapi.Models.dbo.Comment;
import com.experis.tidsbankenapi.Models.dbo.Request;
import com.experis.tidsbankenapi.Models.dbo.RequestStatus;
import com.experis.tidsbankenapi.Models.dbo.User;
import com.experis.tidsbankenapi.Repositories.ICommentRepository;
import com.experis.tidsbankenapi.Repositories.IRequestRepository;
import com.experis.tidsbankenapi.Repositories.IUserRepository;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin("${server.cors.application_origin}")
@SecurityRequirement(name = "keycloak_implicit")
@RequestMapping("api")
public class RequestController {

    private final IRequestRepository requestRepository;
    private final IUserRepository userRepository;
    private final ICommentRepository commentRepository;

    public RequestController(IRequestRepository requestRepository, IUserRepository userRepository, ICommentRepository commentRepository) {
        this.requestRepository = requestRepository;
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
    }

    @GetMapping("/request")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Page<Request>> getRequest(@AuthenticationPrincipal Jwt principal,
                                                    @RequestParam(defaultValue = "0", required = false) Integer page) {
        Pageable pageable = PageRequest.of(page,50);
        if(!principal.getClaimAsStringList("roles").contains("admin")){
            //Returns all requests for user no matter what status and all approved requests for other users
            Page<Request> requestList = requestRepository.getAllByUserOrRequestStatusOrderByPeriodStartAsc(userRepository.getUserById(UUID.fromString(principal.getSubject())), RequestStatus.APPROVED, pageable);
            if(requestList.isEmpty()){
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(requestList);

        }
        Page<Request> requestList = requestRepository.OrderByPeriodStartAsc(pageable);
        if(requestList.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(requestList);
    }

    @PostMapping("/request")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Request> createRequest(@RequestBody Request newRequest, @Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){
        Request createdRequest = new Request();
        createdRequest.setTitle(newRequest.getTitle());
        createdRequest.setPeriodStart(newRequest.getPeriodStart());
        createdRequest.setPeriodEnd(newRequest.getPeriodEnd());
        createdRequest.setRequestStatus(RequestStatus.PENDING);
        createdRequest.setUser(userRepository.getUserById(UUID.fromString(principal.getSubject())));
        createdRequest.setSeen(false);
        requestRepository.save(createdRequest);

        return ResponseEntity.ok(createdRequest);
    }

    @GetMapping("/request/{requestId}")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Request> getRequestById(@PathVariable Integer requestId, @Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){
        Optional<Request> searchResult = requestRepository.findById(requestId);
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        Request request = searchResult.get();
        requestRepository.save(request);
        if(!principal.getClaimAsStringList("roles").contains("admin") && !userRepository.getUserById(UUID.fromString(principal.getSubject())).equals(request.getUser())){
            if(request.getRequestStatus().equals(RequestStatus.APPROVED)){
                return ResponseEntity.ok(searchResult.get());
            }
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok(searchResult.get());
    }

    @PatchMapping("/request/{requestId}")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Request> updateRequestById(@PathVariable Integer requestId, @RequestBody Request patchRequest, @Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){

        Request request = requestRepository.getRequestById(requestId);
        User moderator = userRepository.getUserById(UUID.fromString(principal.getSubject()));
        //If moderator is a user and owns the request and the status has not been modified
        if(!moderator.isAdmin() && moderator.equals(request.getUser()) && request.getRequestStatus().equals(RequestStatus.PENDING)){
            if(patchRequest.getRequestStatus() != null){
                return  new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
            if(patchRequest.getTitle() != null){
                request.setTitle(patchRequest.getTitle());
            }
            if(patchRequest.getPeriodStart() != null){
                request.setPeriodStart(patchRequest.getPeriodStart());
            }
            if(patchRequest.getPeriodEnd() != null){
                request.setPeriodEnd(patchRequest.getPeriodEnd());
            }
            requestRepository.save(request);
            return ResponseEntity.ok(requestRepository.getById(requestId));
        }
        if(moderator.isAdmin()){
            if(patchRequest.getRequestStatus() != null){
                request.setRequestStatus(patchRequest.getRequestStatus());

            }
            if(patchRequest.getTitle() != null){
                request.setTitle(patchRequest.getTitle());
            }
            if(patchRequest.getPeriodStart() != null){
                request.setPeriodStart(patchRequest.getPeriodStart());
            }
            if(patchRequest.getPeriodEnd() != null){
                request.setPeriodEnd(patchRequest.getPeriodEnd());
            }
            request.setModerator(moderator);
            request.setTimeOfModeration(new Date());
            request.setSeen(false);
            requestRepository.save(request);
            return ResponseEntity.ok(requestRepository.getById(requestId));

        }
        return new ResponseEntity("User not allowed to make this change, request status is PENDING or user is not the owner of request",HttpStatus.METHOD_NOT_ALLOWED);
    }

    @DeleteMapping("request/{requestId}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Boolean> deleteRequestById(@PathVariable Integer requestId){
        Request request = requestRepository.getRequestById(requestId);
        List<Comment> requestComments= commentRepository.getAllByRequestIdOrderByTimeStampDesc(requestId);
        for(Comment comment: requestComments){
            commentRepository.delete(comment);
        }
        requestRepository.delete(request);
        return ResponseEntity.ok(true);
    }



}