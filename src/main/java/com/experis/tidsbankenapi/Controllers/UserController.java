package com.experis.tidsbankenapi.Controllers;

import com.experis.tidsbankenapi.Models.dbo.*;
import com.experis.tidsbankenapi.Repositories.ICommentRepository;
import com.experis.tidsbankenapi.Repositories.IRequestRepository;
import com.experis.tidsbankenapi.Repositories.IUserRepository;
import com.experis.tidsbankenapi.Services.UserService;
import com.nimbusds.jose.shaded.json.JSONObject;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;


@RestController
@CrossOrigin("${server.cors.application_origin}")
@SecurityRequirement(name = "keycloak_implicit")
@RequestMapping("api")
public class UserController {

    private final IUserRepository userRepository;
    private final UserService userService;
    private final IRequestRepository requestRepository;
    private final ICommentRepository commentRepository;

    public UserController(IUserRepository usersRepository, UserService userService, IRequestRepository requestRepository, ICommentRepository commentRepository) {
        this.userRepository = usersRepository;
        this.userService = userService;
        this.requestRepository = requestRepository;
        this.commentRepository = commentRepository;
    }

    @GetMapping("/user")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<String> getUser(@AuthenticationPrincipal Jwt principal) throws URISyntaxException {
        User user = userRepository.getUserById(UUID.fromString(principal.getSubject()) );
        URI location = new URI(String.format("https://tidsbanken-api.herokuapp.com/api/user/%s", user.getId()));
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        headers.setBearerAuth(String.valueOf(principal));
        return new ResponseEntity<>(headers,HttpStatus.SEE_OTHER);
    }

    @GetMapping("/user/all")
    @PreAuthorize("hasAnyRole('admin')")
    public ResponseEntity<List<User>> getAllUsers(@AuthenticationPrincipal Jwt principal) {
        List<User> searchResult = userRepository.findAll();
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult);
    }



    @GetMapping("/user/{userId}")
    //@PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity getUserById(@AuthenticationPrincipal Jwt principal , @PathVariable UUID userId){
        Optional<User> searchResult = userRepository.findUserById(userId);
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }

        if(!principal.getClaimAsStringList("roles").contains("admin") && !UUID.fromString(principal.getSubject()).equals(searchResult.get().getId())){
            JSONObject responsebody = new JSONObject();
            responsebody.put("email",searchResult.get().getEmail());
            responsebody.put("firstName",searchResult.get().getFirstName());
            responsebody.put("lastName",searchResult.get().getLastName());
            return ResponseEntity.ok().body(responsebody);
        }
        return ResponseEntity.ok(searchResult.get());
    }

    @GetMapping("/user/{userId}/requests")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<List<String>> getUserRequest(@PathVariable UUID userId){
        List<String> searchResult = userRepository.findUserById(userId).get().getRequests();
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult);
    }

    @PostMapping("/user")
    @PreAuthorize("hasAnyRole('admin')")
    public ResponseEntity<String> createUser(@RequestBody User newUser){
        if(!userService.checkIfUserExists(newUser.getId())){
            User createdUser = new User();
            createdUser.setId(newUser.getId());
            createdUser.setEmail(newUser.getEmail());
            createdUser.setFirstName(newUser.getFirstName());
            createdUser.setLastName(newUser.getLastName());
            createdUser.setAdmin(newUser.isAdmin());
            userRepository.save(createdUser);
            return new ResponseEntity<String>("User created!",HttpStatus.CREATED);
        }
        return ResponseEntity.ok("User already exists!");
    }

    @DeleteMapping("/user/{userId}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Boolean> deleteUserById(@PathVariable UUID userId) {
        User user = userRepository.getUserById(userId);
        List<Request> userRequests = requestRepository.getAllByUser(user);
        for(Request request:userRequests){
            List<Comment> requestComments = commentRepository.getAllByRequest(request);
            for(Comment comment: requestComments){
                commentRepository.delete(comment);
            }
            requestRepository.delete(request);
        }
        userRepository.delete(user);
        return ResponseEntity.ok(true);
    }

    @PatchMapping("/user/{userId}")
    @PreAuthorize("hasAnyRole({'user', 'admin'})")
    public ResponseEntity<User> updateUserById(@PathVariable UUID userId, @RequestBody User patchUser, @Parameter(hidden = true) @AuthenticationPrincipal Jwt principal) {

            User moderator = userRepository.getUserById(UUID.fromString(principal.getSubject()));
            User user = userRepository.getUserById(userId);

            if(moderator.isAdmin()) {

                if (patchUser.getFirstName() != null) {
                    user.setFirstName(patchUser.getFirstName());
                }
                if (patchUser.getLastName() != null) {
                    user.setLastName(patchUser.getLastName());
                }
                if (patchUser.getEmail() != null) {
                    user.setEmail(patchUser.getEmail());
                }
                if (patchUser.isAdmin()) {
                    user.setAdmin(patchUser.isAdmin());
                }
                if(patchUser.getLastLogin() != null){
                    user.setLastLogin(patchUser.getLastLogin());
                }
                userRepository.save(user);
                return ResponseEntity.ok(userRepository.getUserById(userId));
            }
            if (!moderator.isAdmin() && moderator.equals(user)) {
                if (patchUser.getFirstName() != null) {
                    user.setFirstName(patchUser.getFirstName());
                }
                if (patchUser.getLastName() != null) {
                    user.setLastName(patchUser.getLastName());
                }
                if (patchUser.getEmail() != null) {
                    user.setEmail(patchUser.getEmail());
                }
                if(patchUser.getLastLogin() != null){
                    user.setLastLogin(patchUser.getLastLogin());
                }
                userRepository.save(user);
                return ResponseEntity.ok(userRepository.getUserById(userId));
            }
        return new ResponseEntity("Only admin are allowed to set user admin status",HttpStatus.METHOD_NOT_ALLOWED);
    }

    @GetMapping("/user/{userId}/notifications")
    @PreAuthorize("hasAnyRole({'user', 'admin'})")
    public ResponseEntity<List<Notification>> getUserNotifications(@PathVariable UUID userId, @Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){
        User user = userRepository.getUserById(userId);
        Date timestamp = user.getLastLogin();
        List<Notification> notifications = new ArrayList<>();
        //List<Request> requests = requestRepository.getAllByTimeOfModerationIsAfterAndUser(timestamp, user);
        //List<Comment> comments = commentRepository.getAllByTimeStampAfterOrEditedAtTimeAfterAndRequest_User(timestamp,timestamp, user);
        List<Request> requests = requestRepository.getAllBySeenAndUser(false, user);
        List<Comment> comments = commentRepository.getAllBySeenAndRequest_User(false, user);

        if(requests.isEmpty() && comments.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        //Notification notifications = new Notification(requests,comments,content);
        for(Request req: requests){
            if(req.isSeen() || req.getModerator().equals(user)){
                continue;
            }
            Notification notification = new Notification(req);
            notification.setType(ContentType.REQUEST);
            notification.setTimeStamp(req.getTimeOfModeration());
            notifications.add(notification);
        };
        for(Comment com: comments){
            if(com.isSeen() || com.getUser().equals(user)){
                continue;
            }
            Notification notification = new Notification(com);
            notification.setType(ContentType.COMMENT);
            if(com.getEditedAtTime() != null){
                notification.setTimeStamp(com.getEditedAtTime());
            }
            else{
                notification.setTimeStamp(com.getTimeStamp());
            }
            notifications.add(notification);
        }
        notifications.sort(Comparator.comparing(Notification::getTimeStamp));
        return ResponseEntity.ok(notifications);
    }

}
