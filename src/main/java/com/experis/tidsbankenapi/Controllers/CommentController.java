package com.experis.tidsbankenapi.Controllers;

import com.experis.tidsbankenapi.Models.dbo.Comment;
import com.experis.tidsbankenapi.Models.dbo.User;
import com.experis.tidsbankenapi.Repositories.ICommentRepository;
import com.experis.tidsbankenapi.Repositories.IRequestRepository;
import com.experis.tidsbankenapi.Repositories.IUserRepository;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@CrossOrigin("${server.cors.application_origin}")
@SecurityRequirement(name = "keycloak_implicit")
@RequestMapping("api")
public class CommentController {

    private final ICommentRepository commentRepository;
    private final IRequestRepository requestRepository;
    private final IUserRepository userRepository;


    public CommentController(ICommentRepository commentRepository, IRequestRepository requestRepository, IUserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.requestRepository = requestRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/request/{requestId}/comment")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<List<Comment>> getCommentByRequestId(@PathVariable Integer requestId, @Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){
        User user = userRepository.getUserById(UUID.fromString(principal.getSubject()));
        if(user.isAdmin() || requestRepository.getById(requestId).getUser().equals(user)){
            List<Comment> commentList = commentRepository.getAllByRequestIdOrderByTimeStampDesc(requestId);
            if(commentList.isEmpty()){
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(commentList);
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @GetMapping("request/{requestId}/comment/{commentId}")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Comment> getCommentByIdAndRequestId(@PathVariable Integer requestId,@PathVariable Integer commentId, @Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){
        User user = userRepository.getUserById(UUID.fromString(principal.getSubject()));
        if(user.isAdmin() || requestRepository.getById(requestId).getUser().equals(user)) {
            Optional<Comment> searchResult = commentRepository.getCommentByRequestIdAndId(requestId,commentId);
            if(searchResult.isEmpty()){
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(searchResult.get());
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @PostMapping("/request/{requestId}/comment")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Comment> createCommentByRequestId(@PathVariable Integer requestId,@RequestBody Comment newComment, @Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){
        //user making request
        User user = userRepository.getUserById(UUID.fromString(principal.getSubject()));
        //if user is admin or owns the request
        if(user.isAdmin() || requestRepository.getById(requestId).getUser().equals(user)) {
            Comment createdComment = new Comment();
            createdComment.setMessage(newComment.getMessage());
            createdComment.setTimeStamp(new Date());
            createdComment.setUser(user);
            createdComment.setRequest(requestRepository.getRequestById(requestId));
            createdComment.setSeen(false);
            commentRepository.save(createdComment);

            return ResponseEntity.ok(createdComment);
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @PatchMapping("/request/{requestId}/comment/{commentId}")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Comment> updateCommentByIdAndRequestId(@PathVariable Integer requestId, @PathVariable Integer commentId,@RequestBody Comment patchComment,@Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){
        User user = userRepository.getUserById(UUID.fromString(principal.getSubject()));
        Comment comment = commentRepository.getCommentByRequestIdAndId(requestId,commentId).get();
        Date timeNow = new Date();
        long hoursSinceCreation = TimeUnit.HOURS.convert(Math.abs((timeNow.getTime() - comment.getTimeStamp().getTime())),TimeUnit.MILLISECONDS);
        if(comment.getUser().equals(user) && (hoursSinceCreation<24)){
            if(patchComment.getMessage() != null){
                comment.setMessage(patchComment.getMessage());
                comment.setEdited(true);
                comment.setEditedAtTime(new Date());
                comment.setSeen(false);
            }
            commentRepository.save(comment);
            return ResponseEntity.ok(comment);
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("/request/{requestId}/comment/{commentId}")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Boolean> deleteCommentByIdAndRequestId(@PathVariable Integer requestId,@PathVariable Integer commentId,@Parameter(hidden = true) @AuthenticationPrincipal Jwt principal){
        User user = userRepository.getUserById(UUID.fromString(principal.getSubject()));
        Comment comment = commentRepository.getCommentByRequestIdAndId(requestId,commentId).get();
        Date timeNow = new Date();
        long hoursSinceCreation = TimeUnit.HOURS.convert(Math.abs((timeNow.getTime() - comment.getTimeStamp().getTime())),TimeUnit.MILLISECONDS);
        if( (user.isAdmin() && hoursSinceCreation<24) || (comment.getUser().equals(user)) && hoursSinceCreation<24){
            commentRepository.delete(comment);
            return ResponseEntity.ok(true);
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
}