package com.experis.tidsbankenapi.Services;

import com.experis.tidsbankenapi.Models.dbo.User;
import com.experis.tidsbankenapi.Repositories.IUserRepository;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {

     private final IUserRepository userRepository;

     public UserService(IUserRepository userRepository) {
          this.userRepository = userRepository;
     }

     public boolean checkIfUserExists(UUID id){
          return userRepository.existsById(id);
     }

     public User createNewUserFromJwt(Jwt principal){
          User newUser = new User();
          newUser.setId(UUID.fromString(principal.getSubject()));
          newUser.setEmail(principal.getClaimAsString("email"));
          newUser.setFirstName(principal.getClaimAsString("given_name"));
          newUser.setLastName(principal.getClaimAsString("family_name"));
          newUser.setAdmin(principal.getClaimAsStringList("roles").contains("admin"));

          newUser = userRepository.save(newUser);

          return newUser;
     }
}
